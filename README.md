Figma page https://www.figma.com/file/njRfuDO7WdruxtZJZi8DuK/SL-koduka-update?node-id=0%3A1
Trello https://trello.com/invite/b/ioNKoN33/e9044bb6cadf5fb8845eb631013d0bfd/kodukas
Tekstid https://concrete-thread-b16.notion.site/Koduka-tekstid-9343cb6fbc0e486ab0d2bac21c1c8c42

# TODO

- [ ] Landing page - Team
- [ ] Landing page - Our competence
- [ ] Landing page - add multilingual options
- [ ] Landing page - Testimonials
- [ ] Landing page - google maps footerisse
- [ ] Kontorist video
- [ ] Case study detail pages
- [ ] Miks meiega liituda + tutvustus page
- [ ] Mobile & tablet views

- [ ] Sisu ja disaini testimine v�liste inimestega
- [ ] Veebi testimine

---

# create-svelte

Everything you need to build a Svelte project, powered by [`create-svelte`](https://github.com/sveltejs/kit/tree/master/packages/create-svelte);

## Creating a project

If you're seeing this, you've probably already done this step. Congrats!

```bash
# create a new project in the current directory
npm init svelte@next

# create a new project in my-app
npm init svelte@next my-app
```

> Note: the `@next` is temporary

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

Before creating a production version of your app, install an [adapter](https://kit.svelte.dev/docs#adapters) for your target environment. Then:

```bash
npm run build
```

> You can preview the built app with `npm run preview`, regardless of whether you installed an adapter. This should _not_ be used to serve your app in production.
